#
# Description: This method checks to see if the vm has been provisioned
#

def get_request_status(request_id)
# Get current provisioning status


# Get Vm request status

  req=$evm.vmdb("MiqProvisionRequest").find_by_id(request_id) rescue nil

  $evm.log(:info," Request_id=#{request_id}")
  $evm.log(:info,"     Request:STATE=#{req.request_state}")
  $evm.log(:info,"     Request:STATUS=#{req.status}")
  $evm.log(:info,"     Request:MESSAGE=#{req.message}")
  req.approve("admin","auto aprove")
  req
end

def set_new_requests(new_requests,req)
  request_state=req.request_state rescue nil
    case request_state
    when 'error'
      $evm.log(:info, "   Resource #{req.id} is in ERROR.")
      new_requests["error"] << req.id
    when 'finished'
      if req.status.downcase == "error"
      then
        $evm.log(:info, "   Resource is provisioned but with error. Exiting.")
        new_requests["error"] << req.id
      else
        $evm.log(:info, "   Resource is finished provisioned. Exiting.")
        new_requests["done"] << req.id
      end
    else
      $evm.log(:info, "   Resource is #{request_state}. Retrying.")
      new_requests["pending"] << req.id
    end
    $evm.log(:info,"   new_requests=#{new_requests.inspect}")
    new_requests
end

begin
  requests = $evm.get_state_var("requests") rescue nil
  new_requests = requests.clone
  new_requests["pending"]=[]
  $evm.log(:info,"CC- requests=#{requests.inspect}")
  requests["pending"].each do|request_id| 
    req = get_request_status(request_id) 
    new_requests=set_new_requests(new_requests, req)
  end
    $evm.root['ae_retry_interval'] = "5.minute"
    $evm.set_state_var("requests",new_requests)
    $evm.root['ae_result'] = 'ok'
    $evm.root['ae_result'] = 'error' if new_requests["error"].count >0
    $evm.root['ae_result'] = 'retry' if new_requests["pending"].count >0

rescue Exception => e  
    $evm.log('info', " ##################################################### ERROR RETRYING #########################")
    $evm.log('info', " err=#{e.message}") rescue nil
    $evm.log('info', " #{e.backtrace.inspect}") rescue nil
    $evm.root['ae_result']         = 'error'
    $evm.root['ae_retry_interval'] = 20.seconds
    exit MIQ_OK
end
