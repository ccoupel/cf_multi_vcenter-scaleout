# determine le Provider Vcenter et le template a partir du JSON
# les clefs attendues dans le JSON sont:
# - providers multi values
# - template multi values au format template:snapshot
# - containers: values of tags set to clusters
# - custom_spec mono value


##### converti les entrées de dialog en entrée d'option
def yaml_data(option)
  @task.get_option(option).nil? ? nil : YAML.load(@task.get_option(option))
end

def get_orig_option(prov=@task,key=nil)
  hash=yaml_data(:parsed_dialog_option)
  result=hash[0]
  result=hash[0][key] rescue nil unless key.nil?
  return result
end

def parsed_dialog_information
  dialog_options_hash = yaml_data(:parsed_dialog_options)
  dialog_tags_hash = yaml_data(:parsed_dialog_tags)
  if dialog_options_hash.blank? && dialog_tags_hash.blank?
    $evm.log(:info, "Instantiating dialog_parser to populate dialog options")
    $evm.instantiate('/Service/Provisioning/StateMachines/Methods/DialogParser')
    dialog_options_hash = yaml_data(:parsed_dialog_options)
    dialog_tags_hash = yaml_data(:parsed_dialog_tags)
  end

  $evm.log(:info, "dialog_options: #{dialog_options_hash.inspect}")
  $evm.log(:info, "tag_options: #{dialog_tags_hash.inspect}")
  return dialog_options_hash, dialog_tags_hash
end

def get_options_from_dialog()
  @task = $evm.root['service_template_provision_task']
  dialog_options_hash, dialog_tags_hash=parsed_dialog_information
  puts dialog_options_hash
  puts dialog_tags_hash
  
  @task.attributes.each{ |k,v| $evm.log(:info,"CC- @task[#{k}]=#{v}")}
  
  tags={}
  additional_values = {}
  yaml_data(:parsed_dialog_options).each { |x,n| n.each { |k,v| 
    @task.set_option(k.to_s,v)
  }}
  yaml_data(:parsed_dialog_tags).each { |x,n| n.each { |k,v| 
    $evm.log(:info," Tag[#{k.to_s}]=#{v}")
    @task.add_tag(k.to_s,v.downcase) rescue @task.tag_assign("#{k.to_s}/#{v.downcase}")
  }}  
  
end

def get_additional_values(prov,svc)
  opts=prov.options rescue {}
  opts=Hash[prov.options.map { |k,v| [k.to_s,v]}]
  opts[ "direct_service"] = svc.id if opts[ "direct_service"].blank?
    opts.delete("dialog")

  tags={}
  svc.tags.each { |tag| tags[tag.split("/")[0]]=tag.split("/")[1] }
  return opts, tags
end

# construit le tableau de valeurs servant à la creation de la requete de provisionning des VMs
def make_vm_request_skeleton(task=$evm.root['service_template_provision_task'], svc)
prov=task
  
$evm.log(:info, "######################## request definition")
  wanted=prov.get_option("providers") || "NONE"
  wanted_vcenters=wanted.split(",") rescue []
  $evm.log(:info, " JSON wanted vcenters: #{wanted_vcenters}")
    
  wanted=prov.get_option("containers") || "NONE"
  wanted_cluster=wanted.split(",") rescue []
  $evm.log(:info, " JSON wanted conteneur: #{wanted_cluster}")  
  
  wanted=prov.get_option("template") || "NONE"
  wanted_template_snapshot=wanted.split(",") rescue []
  $evm.log(:info, " JSON wanted template: #{wanted_template_snapshot}")  
  
  wanted=prov.get_option("custom_spec") || "NONE"
  wanted_custom_spec=wanted rescue ""
  $evm.log(:info, " JSON wanted custom_spec: #{wanted_custom_spec}") 

$evm.log(:info, "######################## selection of vcenter")
  #search for the 1st PROVIDER availlable
  vcenter=nil
  wanted_vcenters.each do |provider|
    $evm.log(:info, "     searching provider: #{provider}")
    vcenter=$evm.vmdb(:ems_vmware).find_by_name(provider.strip)
      $evm.log(:info, "     checking if #{provider} is elligible: #{vcenter}")
    break unless vcenter.nil?
  end

  final_vcenter=vcenter
  $evm.log(:info,"   vcenter set to #{final_vcenter}")
  ($evm.create_notification(:level => "error", :message => "provider in #{wanted_vcenters} can not be found");raise) if final_vcenter.nil?

$evm.log(:info, "######################## definition of cluster") 
  # search for the cluster with the specified tags
  clusters=final_vcenter.ems_clusters
  cluster=nil
  clusters.each do |c|
    wanted_cluster.each do |wc|
      $evm.log(:info, "     checking if #{c.name} (#{c.tags}) is tagged with #{wc} => #{c.tagged_with?("conteneur",wc)}")
      cluster=c if c.tagged_with?("conteneur",wc)
      break unless cluster.nil?
    end
    break unless cluster.nil?
  end
  final_cluster=cluster
  $evm.log(:info, "     #{final_cluster.name} (#{final_cluster.tags}) is tagged with #{wanted_cluster}")
  ($evm.create_notification(:level => "error", :message => "#{clusters} can not be found with #{final_vcenter}");raise) if final_cluster.nil?


$evm.log(:info, "######################## definition of template")
# search for the template name availlable in the selected cluster
  $evm.log(:info," looking for template: #{wanted_template_snapshot}")
  templates=final_vcenter.miq_templates

    template=nil  
    wanted_snapshot=nil
    wanted_template=nil
  wanted_template_snapshot.each do |wtc|
    wanted_template=wtc.split(":")[0] rescue ""
    wanted_snapshot=wtc.split(":")[1] rescue ""
    $evm.log(:info, " searching for #{wanted_template} in #{final_cluster.name}@#{final_vcenter.name} in (#{templates.count} templates)")

    templates.each do |t|
      $evm.log(:info, "     checking if '#{wanted_template}' == '#{t.name}' (#{t.id}) => #{t.name.casecmp( wanted_template)}")
      
      if (t.name.casecmp(wanted_template.downcase) == 0)
        template=t #if (final_cluster.name.casecmp(t.ems_cluster.name))
      end
      break unless template.nil?
    end
    $evm.log(:info, "              template=#{template}")
    break unless template.nil?
  end
  final_template=template

  $evm.log(:info,"   template set to #{final_template.name} (#{final_template.id}) snapshot=#{wanted_snapshot}")
  ($evm.create_notification(:level => "error", :message => "template in #{wanted_template_snapshot} can not be found with #{final_cluster}");raise) if final_template.nil?

$evm.log(:info, "######################## definition of linked snapshot")
  #search if the snapshot exeists
  final_snapshot=nil
  $evm.log(:info, " template with linked clone: #{wanted_snapshot}")
  unless wanted_snapshot.nil?
    $evm.log(:info,"   looking for #{wanted_snapshot} in #{final_template.snapshots.count} snapshots")
    final_template.snapshots.each do |snap|
      $evm.log(:info, "   checking if #{wanted_snapshot} == #{snap.name} (#{snap.id})")
      final_snapshot=snap if wanted_snapshot==snap.name
      break unless final_snapshot.nil?
    end
  end

  $evm.log(:info, " set template with linked clone:  #{final_snapshot.name rescue nil} (#{final_snapshot.id rescue nil})")  
  ($evm.create_notification(:level => "error", :message => "snashot #{wanted_snapshot} can not be found with #{final_cluster} and #{final_template}");raise) if final_template.nil?
  
$evm.log(:info, "######################## definition of custom spec")
  # search for the custom spec
  $evm.log(:info," looking for customization spec: #{wanted_custom_spec}")

  custom_specs=final_vcenter.customization_specs
  $evm.log(:info, " searching for custom_spec #{wanted_custom_spec} in (#{custom_specs.count} custom_specs)")

  custom_spec=nil
  custom_specs.each do |c|
    $evm.log(:info, "     checking if #{wanted_custom_spec} == #{c.name} (#{c.id})")
    custom_spec=c if (c.name.casecmp(wanted_custom_spec) == 0)
    break unless custom_spec.nil?
  end

  final_custom_spec=custom_spec
  $evm.log(:info,"   custom_spec set to #{final_custom_spec.name} (#{final_custom_spec.id})")
  ($evm.create_notification(:level => "error", :message => "customization spec in #{wanted_custom_spec} can not be found with #{final_cluster}");raise) if final_template.nil?

$evm.log(:info, "######################## final definition")
    $evm.log(:info," final provider: #{wanted_vcenters} => #{final_vcenter.id rescue nil}:#{final_vcenter.name rescue nil}")
    $evm.log(:info," final template: #{wanted_template} => #{final_template.id rescue nil}:#{final_template.name rescue nil}")
    $evm.log(:info," final snapshot:  #{wanted_snapshot}  => #{final_snapshot.id rescue nil}:#{final_snapshot.name rescue nil}")
    $evm.log(:info," final linked clone:  => #{!final_snapshot.nil?}")
    $evm.log(:info," final custom spec id: #{wanted_custom_spec} => #{final_custom_spec.id rescue nil}:#{final_custom_spec.name rescue nil}")
    $evm.log(:info," final spec override:  => #{!final_custom_spec.nil?}")

user=$evm.root["user"]
$evm.log(:info,"  requester user=>#{user.inspect}")
  
$evm.log(:info, "######################## request construction")
additional_values, tags = get_additional_values(task,svc)

request={"version"=>["1.1"],
 "template_fields"=>
  {"guid"=>final_template.guid,
   "name"=>final_template.name,
   "request_type"=>"template"},
 "vm_fields"=>
  {"sysprep_organization"=>"XXX",
   "sysprep_custom_spec"=>final_custom_spec.id,
   "sysprep_server_license_mode"=>"perServer",
   "sysprep_timezone"=>"105",
   "sysprep_identification"=>"workgroup",
   "sysprep_per_server_max_connections"=>"5",
   "sysprep_workgroup_name"=>"WORKGROUP",
   "sysprep_spec_override"=>!final_custom_spec.nil?,
   "addr_mode"=>"dhcp",
   "sysprep_change_sid"=>true,
   "sysprep_enabled"=>"fields",
   "sysprep_full_name"=>"XXX",
   "placement_auto"=>true,
   "number_of_vms"=>0,
   "vm_name"=>"changeme",
   "provision_type"=>"vmware",
   "linked_clone"=>!final_snapshot.nil?,
   "snapshot"=>(final_snapshot.id rescue nil),
   "vm_auto_start"=>false,
   "retirement"=>0,
   "retirement_warn"=>604800,
   "vlan"=>"000",
   "disk_format"=>"unchanged",
   "cpu_limit"=>-1,
   "memory_limit"=>-1,
   "number_of_sockets"=>prov.get_option("number_of_sockets") || 0,
   "cores_per_socket"=>prov.get_option("cores_per_socket") || 0,
   "cpu_reserve"=>0,
   "vm_memory"=> prov_get_option("vm_memory") || "0",
   "memory_reserve"=>0,
   "network_adapters"=>1,
    },
 "requester"=>
    {"owner_email"=>user.email,
     "user_name"=> user.userid ,
     "owner_first_name"=>user.first_name,
     "owner_last_name"=>user.last_name,
     "auto_approve" => true},
 "tags"=>tags||{},
 "additional_values"=>additional_values || {} ,
 "ems_custom_attributes"=>{},
 "miq_custom_attributes"=>{}}
  
  $evm.log(:info," WS-VALUES=#{request["additional_values"]}")
  $evm.log(:info," TAGS=#{request["tags"]}")
  
  request
end

# execute les X requetes de provisioning de VM
def order_request(number_of_vms, request)
    $evm.log(:info, "######################## request ordering")
    request["vm_fields"]["number_of_vms"]=1 if number_of_vms.to_i >0
    requests= { "pending" => [], "done" => [], "error" => [] }
#  request_id=nil

    for i in 1..number_of_vms.to_i do
      request["additional_values"]["vm_id"]=i
      args=request["version"] 
      args << request["template_fields"]
      args << request["vm_fields"]
      args << request["requester"]
      args << request["tags"]
      args << request["additional_values"]
      args << request["ems_custom_attributes"]
      args << request["miq_custom_attributes"]
      args.each_with_index { |v1,index| $evm.log(:info,"CC- args[#{index}]=>#{v1.inspect}") }
      begin
        
        result=$evm.execute('create_provision_request', *args)
        
      rescue Exception => e
        requests["error"] << i 
        $evm.log('warning', " vm=#{i} ERROR=#{e.message}") rescue nil
        $evm.log('info', " #{e.backtrace.inspect}") rescue nil
        next
      end
      requests["pending"] << result.id unless result.nil?
      $evm.log(:info," vm=#{i} requests=#{requests.inspect}")
    end

    $evm.set_state_var("requests",requests) rescue nil   
  requests
end

begin
  $evm.log('info', " starting creation VM") rescue nil
  args=nil
  
  # Appel a partir d'un scaleout
  if $evm.root["vmdb_object_type"] == "service"

    svc=$evm.root["service"]
        $evm.log('info', " creating from service #{svc}")
    
    task = $evm.vmdb(:service_template_provision_task,svc.get_dialog_option("service_template_provision_task_id")) rescue nil
    task = task || $evm.vmdb(:service_template_provision_task).find_by( :destination => svc.id)
    args=svc.get_dialog_option('service_template_provision_task_vm_skeleton') || make_vm_request_skeleton(task,svc)

    number_of_vms=1
    
  # appel pour le provisionning d'un service
  elsif $evm.root["vmdb_object_type"] == "service_template_provision_task"
    # on transpose les entrées de dialog en options
    get_options_from_dialog()
        $evm.log('info', " creating from #{$evm.root["vmdb_object_type"]} #{$evm.root[$evm.root["vmdb_object_type"]]}")
    task=$evm.root[$evm.root["vmdb_object_type"]]
    svc=$evm.vmdb(task["destination_type"],task["destination_id"])
    $evm.log('info', " creating VM for #{svc}")
    
    # construction du Hash de creation de VM
    args=make_vm_request_skeleton(task,svc)
    
    #On sauvegarde les information pour un futur scaleout
    svc.set_dialog_option('service_template_provision_task_vm_skeleton',args)
    svc.set_dialog_option('service_template_provision_task_id',task.id)
    
    number_of_vms=$evm.root[$evm.root["vmdb_object_type"]].get_option("number_of_vms")
  end
    
  #on execute la creation des VMs
    requests=order_request(number_of_vms, args) 

    $evm.log('info', " done #{requests}") rescue nil
    $evm.root['ae_result']         = 'ok'
    $evm.root['ae_result']         = 'error' if requests.nil?
    $evm.root['ae_retry_interval'] = 40.seconds
rescue Exception => e  
    $evm.log('info', "##################################################### ERROR #########################")
    $evm.log('info', " ERROR=#{e.message}") rescue nil
    $evm.log('info', " ERROR=#{e.backtrace.inspect}") rescue nil
    $evm.root['ae_result']         = 'error'
    $evm.root['ae_retry_interval'] = 20.seconds
    exit MIQ_OK
end
