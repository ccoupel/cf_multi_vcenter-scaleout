# Multi Provider Provisionning #
Access: confirmed
Fomat: datastore

## Problematics: ##
When provisioning VMs in a multi vcenter architecture, It could be usefull to selectionne the provider based on several criterias instead of selecting a catalogitem based on only one provider.
For Example, provisioning a service with more than 4 VMs, with a quality of service of Gold, will provision 2 VMs on Provider 1 and 2 VMs on provider2 for sharing risques.

When provisioning a service with multiples VMs, the service catalog item create a service AND the associated VMs. all provisioning messages are pushed in the global request, making difficult to follow the provisioning status of each VMs.

## Solution: ##
Instead of using a VMware catalog item, we will use a generic catalog item and do the creations manually:
> * We create the service with /Service/Provisioning/StateMachines/Methods/Provision
>> This is the native method
> * We create each VM individually /service/Provisioning/StateMachines/Methods/vm_provision
> > 1. From the dialog parameters, we extracts the options and the tags
> > 1. We test each entries of “providers” to select the first one that exists
> > 1. We test each clusters  of the provider to select the first one that is tagged with “conteneur”
> > 1. We test each templates of the provider to find one named “template”
> > 1. We test each snapshot of the template to find one named “template:snapshot”
> > 1. We test each custom_spec of the provider to find one named “custom_spec”
> > 1. It is not possible to pass options to the vm provision request, but it is possible to pass additional values, so we pass them that way
> > 1. We fill the provision hash template with the retrieve values
> > 1. We create as much requests as many needed vms
> > 1. We save the vm request IDs in a Hash for a latter use
> * We wait for the service to be created with /Service/Provisioning/StateMachines/Methods/CheckProvisioned
>> This is the native method
> * We wait for the vms to be created and attach them to the service with /Infrastructure/VM/Provisioning/StateMachines/Methods/CheckVmRequestProvisioned
> > 1. From the request Ids Hash, we check the status of all 
> > 1. Once none of them are pending, the provisionning is finished

Each, service and vms, have their own request that can be followed via the 

## Bonus: ##
As VMs are requested individually, it is easy to add an other identical VM, scale out an existing service without any user input,  To do so add a custom button on service:
System/Process: Request
Message: create
Request: Call_instance_With_Message
Attributes:
> namespace: Service
> class: Provisioning/StateMachines/ServiceProvision_Template
> instance: CatalogItemInitialization_vcenter_selection
> message: scaleout